<?php
require_once 'modelo/recurso.php';
require_once 'vistas/VistaRecursos.php';
require_once 'vistas/VistaRecurso.php';
require_once 'contenedores/contenedorRecurso.php';

$contenedor = new ContenedorRecurso();

// Mostramos el formulario
$tipos = $contenedor->getTipos();

// Vemos si nos han mandado filtros
$id_sala = filter_input(INPUT_GET, "id_sala");
$tipo_recurso = filter_input(INPUT_GET, "tipo");
$accion = filter_input(INPUT_GET, "accion");

$recursos = null;

echo "<div class='cont-secc-recursos'>";
VistaRecursos::formularioFiltroRecursos($tipos);

// Botón de acción
if ($accion == "update"){
    $id_recurso = filter_input(INPUT_GET, "id");
    $id_sala = filter_input(INPUT_GET,"id_sala");
    $info = filter_input(INPUT_GET,"info");
    $figura = filter_input(INPUT_GET,"figura");
    
    if ($contenedor->updateRecurso($id_recurso, $id_sala, $info, $figura)){
        VistaRecurso::mensajeExito('Recurso con id ('.$id_recurso.') modificado correctamente');
    }else{
        VistaRecurso::mensajeError('Error al modificar el recurso con id ('.$id_recurso.')');
    } 
}else if ($accion == "delete"){
    $id_recurso = filter_input(INPUT_GET, "id");
    if ($contenedor->deleteRecurso($id_recurso)){
         VistaRecurso::mensajeExito('Recurso con id ('.$id_recurso.') eliminado correctamente');
    }else{
        VistaRecurso::mensajeError('Error al eliminar el recurso con id ('.$id_recurso.')');
    }
}else if ($accion == "new"){
    $tipo = filter_input(INPUT_GET,"tipo");
    $id_sala = filter_input(INPUT_GET,"id_sala");
    $info = filter_input(INPUT_GET,"info");
    $figura = filter_input(INPUT_GET,"figura");
    
    if ($id_recurso = $contenedor->insertRecurso($tipo,$id_sala,$info, $figura)){
        VistaRecurso::mensajeExito('Recurso con id ('.$id_recurso.') creado correctamente');
    }else{
        VistaRecurso::mensajeError('Error al isnertar un nuevo recurso');
    }
}else{
    if ($id_sala && !is_null($id_sala) && $id_sala != ""){
        if ($tipo_recurso && !is_null($tipo_recurso) && $tipo_recurso != ""){
            $recursos = $contenedor->getRecursos($id_sala,$tipo_recurso);
        }else{
            $recursos = $contenedor->getRecursos($id_sala);
        }
    }else if($tipo_recurso && !is_null($tipo_recurso) && $tipo_recurso != ""){
        $recursos = $contenedor->getRecursos(null,$tipo_recurso);
    }else{
        $recursos = $contenedor->getRecursos(null,null);
    }
}

if (is_null($recursos))
    $recursos = $contenedor->getRecursos(null,null);

VistaRecursos::muestraRecursosTabla($recursos,$tipos);

echo "</div>";

