<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include 'contenedores/contenedorRecurso.php';

$id = filter_input(INPUT_POST, "id_recurso");
$file = filter_input(INPUT_POST,"im_qr");

if ($id != null && $id != "" && $file != null && $file != ""){
    $db = new ContenedorRecurso();
    $db->modificarCodigo($id, $file);
}

// Redirigimos al controlador de editarSala
header('Location: index.php?section=editarSala&id_sala='.$id);