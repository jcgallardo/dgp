<?php

include "modelo/sala.php";
include "modelo/recurso.php";
include "modelo/multimedia.php";
include "modelo/RecursoIdioma.php";
include "vistas/VistaEditarSala.php";
include "vistas/VistaRecurso.php";
include "vistas/VistaRecursoIdioma.php";
include "vistas/VistaMultimedia.php";
include "contenedores/contenedorSalas.php";
include 'contenedores/contenedorRecurso.php';
include 'contenedores/contenedorRecursoIdioma.php';
include 'contenedores/contenedorMultimedia.php';

$contenedor = new ContenedorSalas();

$id_sala = filter_input(INPUT_GET, 'id_sala');

if (!is_null($id_sala) && $id_sala){
    $sala = $contenedor->getSalaById($id_sala);
    $boton = filter_input(INPUT_GET,'boton');
    
    if (!is_null($boton) && $boton){
        $id_recurso = filter_input(INPUT_GET, 'id_recurso');
        
        if (!is_null($id_recurso) && $id_recurso){
            $contenedorRecurso = new ContenedorRecurso();
            $contenedorRecurso->eliminarRecurso($id_recurso);
        } 
    }
    
    $_SESSION['id_sala']=$id_sala;
    VistaEditarSala::muestraSala($sala);
}else{
    echo "ERROR --> campo id_sala no encontrado o con valor incorrecto";
}

