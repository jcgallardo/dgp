<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include 'contenedores/contenedorRecurso.php';
$conexion = new Conexion();
$conn = $conexion->getConexion();

$id = filter_input(INPUT_GET, "id_medio");
$id_sala = filter_input(INPUT_GET, "id_sala");

if ($id != null && $id != "" ){
    $db = new ContenedorRecurso();
    $db->eliminarMedio($id);
}

// Redirigimos al controlador de editarSala
header('Location: index.php?section=editarSala&id_sala='.$id_sala);