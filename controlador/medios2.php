<?php
require_once 'modelo/multimedia.php';
require_once 'vistas/VistaMultimedias.php';
require_once 'vistas/VistaMultimedia.php';
require_once 'contenedores/contenedorMultimedia.php';

$contenedor = new ContenedorMultimedia();

// Mostramos el formulario
$idiomas = $contenedor->getIdiomas();
$tipos = $contenedor->getTipos();

// Vemos si nos han mandado filtros
$idioma = filter_input(INPUT_GET, "idioma");
$tipo = filter_input(INPUT_GET, "tipo");
$accion = filter_input(INPUT_GET, "accion");

$recursos = null;

echo "<div class='cont-secc-multimedias'>";
VistaMultimedias::formularioFiltroMultimedias($idiomas,$tipos);

$medios = null;

if ($accion == "update"){
    $id = filter_input(INPUT_GET, "id");
    $id_recurso = filter_input(INPUT_GET, "id_recurso");
    $contenido = filter_input(INPUT_GET,"contenido");
    $descripcion = filter_input(INPUT_GET,"descripcion");
    
    if ($contenedor->updateMedio($id, $id_recurso, $contenido, $descripcion)){
        VistaMultimedia::mensajeExito('Medio con id ('.$id.') modificado correctamente');
    }else{
        VistaMultimedia::mensajeError('Error al modificar el medio con id ('.$id.')');
    } 
}else if ($accion == "delete"){
    $id_medio = filter_input(INPUT_GET, "id");
    if ($contenedor->deleteMedio($id_medio)){
         VistaMultimedia::mensajeExito('Medio con id ('.$id_medio.') eliminado correctamente');
    }else{
        VistaMultimedia::mensajeError('Error al eliminar el medio con id ('.$id_medio.')');
    }
}else if ($accion == "new"){
    $tipo = filter_input(INPUT_GET,"tipo");
    $id_sala = filter_input(INPUT_GET,"id_sala");
    $info = filter_input(INPUT_GET,"info");
    $figura = filter_input(INPUT_GET,"figura");
    
    if ($id_recurso = $contenedor->insertRecurso($tipo,$id_sala,$info, $figura)){
        VistaRecurso::mensajeExito('Recurso con id ('.$id_recurso.') creado correctamente');
    }else{
        VistaRecurso::mensajeError('Error al isnertar un nuevo recurso');
    }
}else{
    if ($idioma && !is_null($idioma) && $idioma != ""){
        if ($tipo && !is_null($tipo) && $tipo != ""){
            $medios = $contenedor->getMedios($idioma,$tipo);
        }else{
            $medios = $contenedor->getMedios($idioma);
        }
    }else if($tipo && !is_null($tipo) && $tipo != ""){
        $medios = $contenedor->getMedios(null,$tipo);
    }else{
        $medios = $contenedor->getMedios(null,null);
    }
}

if (is_null($medios))
    $medios = $contenedor->getMedios(null,null);

VistaMultimedias::muestraMultimediasTabla($medios);

echo "</div>";

