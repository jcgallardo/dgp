<?php

include "modelo/multimedia.php";
include "vistas/VistaEditarMedio.php";
include "contenedores/contenedorMultimedia.php";
include "contenedores/contenedorIdioma.php";
include "modelo/idioma.php";
include "modelo/tipoMultimedia.php";
include "contenedores/contenedorTipoMultimedia.php";


$contenedor = new ContenedorMultimedia();
$array = $contenedor->getAllMultimedia();


$array_idioma = array();
$array_tipo = array();

$sql = "select * from idioma";
    global $conn;
    if ($result = $conn->query($sql)) {

            while($row = $result->fetch_row()){
                $codigo = $row[0];
                $idiom = $row[1];
                
                $idioma = new Idioma($codigo, $idiom);
                array_push($array_idioma, $idioma);
            }
    }

$sql = "select * from tipo_multimedia";
    global $conn;
    if ($result = $conn->query($sql)) {

            while($row = $result->fetch_row()){
                $id = $row[0];
                $tipo = $row[1];
                
                $tipoMultimedia = new TipoMultimedia($id, $tipo);
                array_push($array_tipo, $tipoMultimedia);
            }
    }

$html = "<div class='contenedor_salas'>";
$html .= "<div class='editar_sala'>";
$html .= "<div class='imagen_sala' style='background-image:url(http://www.acciona-apd.com/wp-content/uploads/2014/05/Museo-CajaGRANADA-Memoria-de-Andalucia-02comp.jpg)'></div>";
$html .= "<h1>Edición de medios</h1>";
$html .= "<div class='botones'>";
$html .= "<select id='select_idioma'><option value='all'>Idioma</option> ";

    foreach ($array_idioma as $idioma){
            $html .= "<option value='".$idioma->getCodigo()."'>".$idioma->getIdiom()."</option>";
        }    

$html .= "</select>";

$html .= "<select id='select_tipo'> <option value='all'>Preferencia de Visualización</option> ";

    foreach ($array_tipo as $tipoMultimedia){
            $html .= "<option value='".$tipoMultimedia->getId()."'>".$tipoMultimedia->getTipo()."</option>";
        }    

$html .= "</select>";
$html .= "</div>";
$html .= "<button class='btn btn-success boton_nuevo_recurso' type='button' id='filtrar'>Filtrar</button>";
$html .= "<button class='btn btn-success boton_nuevo_recurso' type='button' class='nuevo_recurso' data-toggle='modal' data-target='#emergente'>Añadir Multimedia</button>";
$html .= "<div class='cont_idioma'>";
$html .= "<div class='cont_medios'>";

foreach ($array as $multimedia){
                $html .= VistaEditarMedio::muestraMedio($multimedia);
            }
            
$html .= '</div>';
$html .= '</div>';
$html .= '</div>';
$html .= '</div>';
echo $html;
      

