<?php
    $conn = Connection::getConnection();

    echo "<section>";
    switch($seccion){
        case "salas":
            include "salas.php";
            break;
        case "recursos":
            include "seccion_recursos.php";
            break;
        case "medios":
            include "medios2.php";
            break;
        case "editarSala":
            include "editar_sala.php";
            break;
        case "nuevo_recurso":
            include "nuevo_recurso.php";
            break;
        case "eliminar_recurso":
            include "eliminar_recurso.php";
            break;
        case "eliminar_medio":
            include "eliminar_medio.php";
            break;

    }
    echo "</section>";
    
    Connection::cerrarConexion();