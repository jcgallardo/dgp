<?php

include "modelo/sala.php";
include "modelo/recurso.php";
include "vistas/VistaSalas.php";
include "contenedores/contenedorSalas.php";
include "contenedores/contenedorRecurso.php";
include "contenedores/contenedorMultimedia.php";

$contenedor = new ContenedorSalas();
$accion = filter_input(INPUT_GET, 'accion');

if (!is_null($accion) && $accion){
    $id_sala = filter_input(INPUT_GET, 'id_sala');
    if ($accion == "add"){
        $titulo = filter_input(INPUT_GET, 'titulo');
        $descripcion = filter_input(INPUT_GET, 'descripcion');
        $url = filter_input(INPUT_GET, 'url');
        if ($contenedor->addSala($id_sala, $titulo, $descripcion, $url)){
            VistaSalas::mensajeExito('Sala con id ('.$id_sala.') insertado correctamente');
        }else{
            if ($id_sala != '' && $titulo != '' && $descripcion != '' && $url != ''){
                VistaSalas::mensajeError('Compruebe que la id no esté repetida.');
            }else {
                VistaSalas::mensajeError('Por favor rellene todos los campos');
            }
        }
    }else{
        if (!is_null($id_sala) && $id_sala){
            $contenedor->eliminarSala($id_sala);
        }
    }
}

$salas = $contenedor->getAllSalas();
VistaSalas::muestraSalas($salas);
