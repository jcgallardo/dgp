<?php
    class ContenedorSalas{
        private $array_salas = array();
        
        function getAllSalas(){
            $sql = "select * from sala"; 
            global $conn;
            if ($result = $conn->query($sql)) { 
                while($row =  $result->fetch_row()){
                    $id = $row[0];
                    $nombre = $row[1];
                    $descripcion = $row[2];
                    $imagen = $row[3];
                    
                    $sala = new Sala($id,$nombre,$descripcion,$imagen, null);
                    array_push($this->array_salas, $sala);
                }
            }
            return $this->array_salas;
        }
        
        function getSalas(){
            return $this->array_salas;
        }
        
        function getSalaById($id){
           
            $sql = "select * from sala where id='".$id."'";
            
            global $conn;    
            if ($result = $conn->query($sql)) { 
                while($row =  $result->fetch_row()){
                    $id = $row[0];
                    $nombre = $row[1];
                    $descripcion = $row[2];
                    $imagen = $row[3];
                    $recurso = new ContenedorRecurso();
                    $arrau_recursos = $recurso->getRecursoBySala($id);
                    
                    $sala = new Sala($id,$nombre,$descripcion,$imagen, $arrau_recursos);
                    array_push($this->array_salas, $sala);
                }
            }
            return $sala;
            
           
        }

    public function eliminarSala($id_sala) {
        global $conn;
        $conn->query("delete from sala where id='".$id_sala."';");
    }

    public function addSala($id_sala, $titulo, $descripcion, $url) {
        global $conn;
        return $conn->query("insert into sala (`id`,`nombre`, `descripcion`, `imagen`)  values('".$id_sala."','".$titulo."','".$descripcion."','".$url."');");
    }

}