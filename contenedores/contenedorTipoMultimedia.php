<?php

class ContenedorTipoMultimedia{
    private $array_tipo = array();
    
    function ContenedorTipoMultimedia(){
        $sql = "select * from tipo_multimedia";
            global $conn;  
            if ($result = $conn->query($sql)) { 
                while($row =  $result->fetch_row()){
                    $id = $row[0];
                    $tipo = $row[1];
                    
                    $tipo = new Idioma($id, $tipo);
                    array_push($this->array_tipo, $tipo);
                }
            }
    }
}