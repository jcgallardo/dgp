<?php
    class ContenedorRecurso{
        private $array_recurso = array();
        
        function getRecurso(){
            return $this->array_recurso;
        }
        
        function getRecursoBySala($idSala){
            $sql = "select * from recurso where id_sala = " .$idSala;
            global $conn;
            if ($result = $conn->query($sql)) { 
                while($row =  $result->fetch_row()){
                    $id = $row[0];
                    $informacion = $row[2];
                    $figura = $row[3];
                    
                    $cont_recurso_idioma= new ContenedorRecursoIdioma();
                    $array_recurso_idiomas = $cont_recurso_idioma->getArrayIdIdiomas($id);
                    $recurso = new Recurso($id,$idSala,$informacion,$figura, $array_recurso_idiomas);
                    array_push($this->array_recurso, $recurso);
                }
            }
           
            return $this->array_recurso;
        }
        
        function crearRecurso($idSala, $tit_recurso, $file){
            global $conn;
            $conn->query("insert into recurso (`id_sala`,`informacion`, `figura`)  values('".$idSala."','".$tit_recurso."','".$file."');");
        }
        
        function eliminarRecurso($id_recurso){
            global $conn;
            $conn->query("delete from recurso where id=".$id_recurso.";");
        }
        
        function eliminarMedio($id_medio){
            global $conn;
            $conn->query("update multimedia set id_recurso=null where id='".$id_medio."';");
        }
        
        function modificarCodigo($id_recurso,$file){
            global $conn;
            $conn->query("update recurso set figura='".$file."'where id='".$id_recurso."';");
        }
        
        function getRecursos($id_sala = null, $tipo = null){
            $sql = 'select recurso.id,id_sala,tipo_recurso.tipo,informacion,figura from recurso,tipo_recurso where recurso.id_tipo=tipo_recurso.id order by recurso.id desc;';
            
            if (!is_null($id_sala) && !is_null($tipo)){
                $sql = 'select recurso.id,id_sala,tipo_recurso.tipo,informacion,figura from recurso,tipo_recurso where id_sala='.$id_sala.' AND id_tipo="'.$tipo.'" AND recurso.id_tipo=tipo_recurso.id order by recurso.id desc;';
            }else if (!is_null($tipo)){
                $sql = 'select recurso.id,id_sala,tipo_recurso.tipo,informacion,figura from recurso,tipo_recurso where id_tipo="'.$tipo.'" AND recurso.id_tipo=tipo_recurso.id order by recurso.id desc;';
            }else if (!is_null($id_sala)){
                $sql = 'select recurso.id,id_sala,tipo_recurso.tipo,informacion,figura from recurso,tipo_recurso where id_sala='.$id_sala.' AND recurso.id_tipo=tipo_recurso.id order by recurso.id desc;';
            }
            global $conn;
            $result = $conn->query($sql);
            if ($result->num_rows > 0){
                while($row =  $result->fetch_assoc()){
                    $id = $row['id'];
                    $id_sala = $row['id_sala'];
                    $tipo = $row['tipo'];
                    $informacion = $row['informacion'];
                    $figura = $row['figura'];
                    $recurso = new Recurso($id,$id_sala,$informacion,$figura,null,$tipo);
                    array_push($this->array_recurso, $recurso);
                }
            }
            return $this->array_recurso;
        }
        function getTipos(){
            $array = array();
            global $conn;
            $result = $conn->query("select id, tipo from tipo_recurso;");
            
            if ($result->num_rows > 0){
                while ($row = $result->fetch_assoc()){
                    $array[$row['id']] = $row['tipo'];
                }
            }
            return $array;
        }
        function updateRecurso($id,$id_sala,$info,$figura){
            global $conn;
            $stmt = $conn->prepare("update recurso set id_sala=?, informacion=?, figura=? where id=?");
            $stmt->bind_param("ssss",$id_sala,$info,$figura,$id);
            $stmt->execute();
            
            return $conn->affected_rows;
        }

    public function deleteRecurso($id_recurso) {
        global $conn;
        $stmt = $conn->prepare("delete from recurso where id=?");
        $stmt->bind_param("s",$id_recurso);
        $stmt->execute();

        return $conn->affected_rows;
    }
    public function insertRecurso($tipo,$id_sala,$info,$figura) {
        global $conn;
        $stmt = $conn->prepare("insert into recurso (`id_sala`,`informacion`,`figura`,`id_tipo`) values (?,?,?,?);");
        $stmt->bind_param("ssss",$id_sala,$info,$figura,$tipo);
        $stmt->execute();
            
        return $conn->insert_id;
    }

}
