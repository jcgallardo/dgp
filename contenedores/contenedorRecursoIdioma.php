<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of contenedorRecursoMultimedia
 *
 * @author jcgallardo
 */
class ContenedorRecursoIdioma {
    private $contenedor = array();
    
    public function getArrayIdIdiomas($id_recurso){
        $sql = "select distinct id_idioma,idioma.idiom as idiom from multimedia,idioma where id_recurso='".$id_recurso."' and multimedia.id_idioma=idioma.codigo;";

            global $conn;
            if ($result = $conn->query($sql)) { 
                while($row =  $result->fetch_assoc()){
                    $id_idioma = $row['id_idioma'];
                    $idioma = $row['idiom'];
                    $cont_multimedia = new ContenedorMultimedia();
                    $array_multimedia = $cont_multimedia->getMultimediaByRecursoAndIdioma($id_recurso, $id_idioma);
                    
                    $recurso_idioma = new RecursoIdioma($id_recurso,$id_idioma, $idioma, $array_multimedia);
                    array_push($this->contenedor, $recurso_idioma);
                }
            }
        return $this->contenedor;
           
    }
}
