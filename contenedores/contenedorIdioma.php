<?php

class ContenedorIdioma{
    private $array_idioma = array();
    
    function ContenedorIdioma(){
        $sql = "select * from idioma";
            global $conn;  
            if ($result = $conn->query($sql)) { 
                while($row =  $result->fetch_row()){
                    $codigo = $row[0];
                    $idiom = $row[1];
                    
                    $idioma = new Idioma($codigo, $idiom);
                    array_push($this->array_idioma, $idioma);
                }
            }
    }
}

