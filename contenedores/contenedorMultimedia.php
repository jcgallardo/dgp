<?php
    class ContenedorMultimedia{
        private $array_multimedia = array();
       
        function getMultimediaByRecurso($id_recurso){
            $sql = "select * from multimedia where id_qr = " . $id_recurso;
            global $conn;
            $arrayMultimedia = array();    
            if ($result = $conn->query($sql)) { 
                while($row =  $result->fetch_row()){
                    $id = $row[0];
                    $id_idioma = $row[1];
                    $id_recurso = $row[2];
                    $url = $row[3];
                    $id_tipo = $row[4];
                    $descripcion = $row[5];

                    $multimedia = new Multimedia($id, $id_idioma, $id_recurso, $url, $id_tipo, $descripcion);
                    array_push($arrayMultimedia, $qr);
                }
            }
           
            return $arrayMultimedia;
        }
        function getMultimediaByRecursoAndIdioma($id_recurso, $id_idioma){
            $sql = "select * from multimedia where id_recurso ='".$id_recurso."' and id_idioma='".$id_idioma."';";    
            global $conn;
            if ($result = $conn->query($sql)) { 
                while($row =  $result->fetch_assoc()){
                    $id = $row['id'];
                    $contenido = $row['contenido'];
                    $id_tipo = $row['id_tipo'];
                    $descripcion = $row['descripcion'];

                    $multimedia = new Multimedia($id, $id_idioma, $id_recurso, $contenido, $id_tipo, $descripcion);
                    array_push($this->array_multimedia, $multimedia);
                }
            }
            return $this->array_multimedia;
        }
        function getMultimediaByIdioma($id_idioma){
            $sql = "select * from multimedia where id_idioma='".$id_idioma."';";    
            global $conn;
            if ($result = $conn->query($sql)) { 
                while($row =  $result->fetch_assoc()){
                    $id = $row['id'];
                    $contenido = $row['contenido'];
                    $id_tipo = $row['id_tipo'];
                    $descripcion = $row['descripcion'];

                    $multimedia = new Multimedia($id, $id_idioma, $id_recurso, $contenido, $id_tipo, $descripcion);
                    array_push($this->array_multimedia, $multimedia);
                }
            }
            return $this->array_multimedia;
        }
        function getMultimediaByTipo($id_tipo){
            $sql = "select * from multimedia where id_tipo ='".$id_tipo."';";    
            global $conn;
            if ($result = $conn->query($sql)) { 
                while($row =  $result->fetch_assoc()){
                    $id = $row['id'];
                    $contenido = $row['contenido'];
                    $id_tipo = $row['id_tipo'];
                    $descripcion = $row['descripcion'];

                    $multimedia = new Multimedia($id, $id_idioma, $id_recurso, $contenido, $id_tipo, $descripcion);
                    array_push($this->array_multimedia, $multimedia);
                }
            }
            return $this->array_multimedia;
        }
        function getAllMultimedia(){
            $sql = "select * from multimedia";
            global $conn;  
            if ($result = $conn->query($sql)) { 
                while($row =  $result->fetch_row()){
                    $id = $row[0];
                    $id_idioma = $row[1];
                    $id_recurso = $row[2];
                    //$url = $row[3];
                    $contenido = $row[3];
                    $id_tipo = $row[4];
                    $descripcion = $row[5];
                    //$id_sala = $row[6];

                    $multimedia = new Multimedia($id, $id_idioma, $id_recurso, $contenido, $id_tipo, $descripcion);
                    array_push($this->array_multimedia, $multimedia);
                }
            }
            return $this->array_multimedia;
        }

    public function getTipos() {
        $array = array();
            global $conn;
            $result = $conn->query("select id, tipo from tipo_multimedia;");
            
            if ($result->num_rows > 0){
                while ($row = $result->fetch_assoc()){
                    $array[$row['id']] = $row['tipo'];
                }
            }
            return $array;
    }
    
    public function getIdiomas(){
        $array = array();
            global $conn;
            $result = $conn->query("select codigo, idiom from idioma;");
            
            if ($result->num_rows > 0){
                while ($row = $result->fetch_assoc()){
                    $array[$row['codigo']] = $row['idiom'];
                }
            }
            return $array;
    }
    
    function getMedios($idioma = null, $tipo = null){
            $sql = 'select multimedia.id,idioma.idiom,id_recurso,contenido,tipo_multimedia.tipo,descripcion from multimedia,tipo_multimedia,idioma where multimedia.id_tipo=tipo_multimedia.id and idioma.codigo=multimedia.id_idioma order by multimedia.id desc;';
            
            if (!is_null($idioma) && !is_null($tipo)){
                $sql = 'select multimedia.id,idioma.idiom,id_recurso,contenido,tipo_multimedia.tipo,descripcion from multimedia,tipo_multimedia,idioma where multimedia.id_tipo=tipo_multimedia.id and idioma.codigo=multimedia.id_idioma and id_idioma='.$idioma.' and id_tipo='.$tipo.' order by multimedia.id desc;';
            }else if (!is_null($tipo)){
                $sql = 'select multimedia.id,idioma.idiom,id_recurso,contenido,tipo_multimedia.tipo,descripcion from multimedia,tipo_multimedia,idioma where multimedia.id_tipo=tipo_multimedia.id and idioma.codigo=multimedia.id_idioma and id_tipo='.$tipo.' order by multimedia.id desc;';
            }else if (!is_null($idioma)){
                $sql = 'select multimedia.id,idioma.idiom,id_recurso,contenido,tipo_multimedia.tipo,descripcion from multimedia,tipo_multimedia,idioma where multimedia.id_tipo=tipo_multimedia.id and idioma.codigo=multimedia.id_idioma and id_idioma='.$idioma.' order by multimedia.id desc;';
            }
            global $conn;
            $result = $conn->query($sql);
            if ($result->num_rows > 0){
                while($row =  $result->fetch_assoc()){
                    $id = $row['id'];
                    $idiom = $row['idiom'];
                    $id_recurso = $row['id_recurso'];
                    $contenido = $row['contenido'];
                    $tipo = $row['tipo'];
                    $descripcion = $row['descripcion'];
                    $medio = new Multimedia($id,$idiom,$id_recurso,$contenido,$tipo,$descripcion);
                    array_push($this->array_multimedia, $medio);
                }
            }
            return $this->array_multimedia;
        }

    public function deleteMedio($id_medio) {
        global $conn;
        $stmt = $conn->prepare("delete from multimedia where id=?");
        $stmt->bind_param("s",$id_medio);
        $stmt->execute();

        return $conn->affected_rows;
    }

    public function updateMedio($id, $id_recurso, $contenido, $descripcion) {
        global $conn;
            $stmt = $conn->prepare("update multimedia set id_recurso=?, contenido=?, descripcion=? where id=?");
            $stmt->bind_param("ssss",$id_recurso,$contenido,$descripcion,$id);
            $stmt->execute();
            
            return $conn->affected_rows;
    }

}
        
