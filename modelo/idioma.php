<?php

class Idioma{
    private $codigo;
    private $idiom;
    
    function __construct($codigo, $idiom) {
            $this->codigo = $codigo;
            $this->idiom = $idiom;     
    }
    
    function getCodigo() {
        return $this->codigo;
    }

    function getIdiom() {
        return $this->idiom;
    }

    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    function setIdiom($idiom) {
        $this->idiom = $idiom;
    }
}
