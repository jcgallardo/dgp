<?php
    class Multimedia{
        private $id;
        private $id_idioma;
        private $id_recurso;
        private $contenido;
        private $id_tipo;
        private $descripcion;
        
        function __construct($id, $id_idioma, $id_recurso, $contenido, $id_tipo, $descripcion) {
            $this->id = $id;
            $this->id_idioma = $id_idioma;
            $this->id_recurso = $id_recurso;
            $this->contenido = $contenido;
            $this->id_tipo = $id_tipo;
            $this->descripcion = $descripcion;
        }
        function getId() {
            return $this->id;
        }

        function getId_idioma() {
            return $this->id_idioma;
        }

        function getId_recurso() {
            return $this->id_recurso;
        }

        function getContenido() {
            return $this->contenido;
        }

        function getId_tipo() {
            return $this->id_tipo;
        }

        function getDescripcion() {
            return $this->descripcion;
        }
        function getIdSala(){
            return $this->id_sala;
        }
        function setId($id) {
            $this->id = $id;
        }

        function setId_idioma($id_idioma) {
            $this->id_idioma = $id_idioma;
        }

        function setId_qr($id_qr) {
            $this->id_qr = $id_qr;
        }

        function setUrl($url) {
            $this->url = $url;
        }

        function setId_tipo($id_tipo) {
            $this->id_tipo = $id_tipo;
        }

        function setDescripcion($descripcion) {
            $this->descripcion = $descripcion;
        }
    }
    