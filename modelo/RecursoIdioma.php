<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RecursoIdioma
 *
 * @author jcgallardo
 */
class RecursoIdioma {
    //put your code here
    private $array_multimedia = array();
    private $idioma;
    private $id_idioma;
    private $id_recurso;
    
    public function __construct($id_recurso, $id_idioma, $idioma, $array){
        $this->id_recurso = $id_recurso;
        $this->id_idioma = $id_idioma;
        $this->idioma = $idioma;
        $this->array_multimedia = $array;
    }
    public function getArrayMultimedia(){
        return $this->array_multimedia;
    }
    public function getIdIdioma(){
        return $this->id_idioma;
    }
    public function getIdioma(){
        return $this->idioma;
    }
    public function getIdRecurso(){
        return $this->id_recurso;
    }
    public function setArrayMultimedia($array){
        $this->array_multimedia = $array;
    }
    public function setIdIdioma($id){
        $this->id_idioma = $id;
    }
    public function setIdioma($idioma){
        $this->idioma = $idioma;
    }
    public function setIdRecurso($id){
        $this->id_recurso = $id;
    }
}
