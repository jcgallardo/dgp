 <?php
    class Recurso{
        private $id;
        private $id_sala;
        private $tipo;
        private $informacion;
        private $figura;
        private $array_recurso_idioma;
        
        function __construct ($id = null, $id_sala = null, $informacion = null, $figura = null, $array_recurso_idioma = null, $tipo = null){
            if ($id == null){
                $this->id = 1;
            }else{
                $this->id = $id;
            }if ($id_sala == null){
                $this->id_sala = 1;
            }else{
                $this->id_sala = $id_sala;
            }if ($informacion == null){
                $this->informacion = "Información vacia";
            }else{
                $this->informacion = $informacion;
            }if ($figura == null){
                $this->figura = "Foto vacia";
            }else{
                $this->figura = $figura;   
            }if ($array_recurso_idioma == null){
                $this->array_recurso_idioma=array();
            }else{
                $this->array_recurso_idioma = $array_recurso_idioma;
            }
            if ($tipo == null){
                $this->tipo="DESCONOCIDO";
            }else{
                $this->tipo = $tipo;
            }
            
        }
        function getId() {
            return $this->id;
        }

        function getId_sala() {
            return $this->id_sala;
        }
        function getIdTipo(){
            return $this->tipo;
        }

        function getInformacion() {
            return $this->informacion;
        }

        function getFigura() {
            return $this->figura;
        }
        function setId($id) {
            $this->id = $id;
        }

        function setId_sala($id_sala) {
            $this->id_sala = $id_sala;
        }
        function setIdTipo($id_tipo) {
            $this->id_tipo = $id_tipo;
        }

        function setInformacion($informacion) {
            $this->informacion = $informacion;
        }

        function setFigura($figura) {
            $this->figura = $figura;
        }
        function getArrayRecursosIdioma() {
            return $this->array_recurso_idioma;
        }
        function setRecursosIdioma($recursos_idioma) {
            $this->array_recurso_idioma = $recursos_idioma;
        }

    }

