<?php
    class Sala{
        private $id;
        private $nombre;
        private $descripcion;
        private $imagen;
        private $arrayRecursos;
        
        function Sala($id = null, $nombre = null, $descripcion = null, $imagen = null, $recursos = null){
            if ($id == null)
                $this->id = 1;
            else
                $this->id = $id;
            if ($nombre == null)
                $this->nombre = "Plantilla de sala";
            else
                $this->nombre = $nombre;
            if ($descripcion == null)
                $this->descripcion = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.";
            else
                $this->descripcion = $descripcion;
            if ($imagen == null)
                $this->imagen = "https://www.murciaturistica.es/webs/murciaturistica/fotos/1/museos/22006i5_g.jpg";
            else
                $this->imagen = $imagen;
            if ($recursos == null)
                $this->arrayRecursos = [];
            else
                $this->arrayRecursos = $recursos;
        }
        function getNombre(){
            return $this->nombre;
        }
        function getId(){
            return $this->id;
        }
        function getDescripcion(){
            return $this->descripcion;
        }
        function getImagen(){
            return $this->imagen;
        }
        function setId($otra_id){
            $this->nombre = $otra_id;
        }
        function setNombre($otro_nombre){
            $this->nombre = $otro_nombre;
        }
        function setDescripcion($otra_descripcion){
            $this->descripcion = $otra_descripcion;
        }
        function setImagen($otra_imagen){
            $this->imagen = $otra_imagen;
        }
        function getArrayRecursos() {
            return $this->arrayRecursos;
        }
        function setArrayRecursos($arrayRecursos) {
            $this->arrayRecursos = $arrayRecursos;
        } 
    }
?>