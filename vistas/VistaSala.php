<?php
    class VistaSala{
        static function muestraSala($sala){
            $html = "<div class='sala'>";
                $html .= "<div class='cont_buttons'>";
                $html .= "<form role='form' method='get' action='index.php'>";
                $html .= "<button class='btn btn-warning btn-editar-sala' name='editar_qr' type='button' onclick='redirigir_sala(".$sala->getId().");'>Editar</button>";
                $html .= "<button class='btn btn-danger' name='accion' value='eliminar' type='submit'>Eliminar</button>";
                $html .= "<input type='hidden' name='id_sala' value='".$sala->getId()."' />";
                $html .= "</form>";
                $html .= "</div>";
                $html .= "<h1>". $sala->getId(). ". " .$sala->getNombre() ."</h1>";
                $html .= "<div class='cont_sala'>";
                $html .= "<div class='im_sala'><img alt='imagen de sala' src='".$sala->getImagen()."' width='140' height='120' /></div>";
                $html .= "<div class='desc_sala'><p>".$sala->getDescripcion()."</p></div>";
                $html .= "</div>";
            $html .= "</div>";
            
            return $html;
        }

    public static function formAniadirSala() {
        $html = "<div class='cont-form-sala'>";
                $html .= "<form class='form-sala' role='form' method='get' action='index.php'>";
                    $html .= '<input type="hidden" name="section" value="salas" />';
                    $html .= "<div class='form-group'>";
                        $html .= '<label for="i_id_sala">ID Sala</label>';
                        $html .= '<input name="id_sala" type="text" class="form-control" id="i_id_sala" placeholder="Introduce la id de sala">';
                    $html .=  '</div>';
                    $html .= "<div class='form-group'>";
                        $html .= '<label for="i_tit_sala">Titulo de Sala</label>';
                        $html .= '<input name="titulo" type="text" class="form-control" id="i_tit_sala" placeholder="Título de la imagen">';
                    $html .=  '</div>';
                    $html .= "<div class='form-group'>";
                        $html .= '<label for="id_descripcion_sala">Descripción de Sala</label>';
                        $html .= '<input name="descripcion" type="text" class="form-control" id="id_descripcion_sala" placeholder="Introduce la descripción que quieras darle">';
                    $html .=  '</div>';
                    $html .= "<div class='form-group'>";
                        $html .= '<label for="id_imagen_sala">Imagen de Sala</label>';
                        $html .= '<input name="url" type="text" class="form-control" id="id_imagen_sala" placeholder="URL de la imagen">';
                    $html .=  '</div>';
                    $html .= '<button type="submit" class="btn btn-primary" name="accion" value="add">Añadir unsa Sala </button>'; 
                $html .= "</form>";
            $html .= "</div>";
        return $html;
    }

}
?>