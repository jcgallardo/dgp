<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VistaRecursoIdioma
 *
 * @author jcgallardo
 */
class VistaRecursoIdioma {
    public static function printRecursoIdioma($recurso_idioma){
        $html = "<div class='cont_idioma '>";
        $html .= "<h3>".$recurso_idioma->getIdioma()."</h3>";
        $html .= "<div class='cont_medios'>";
            $a_multimedia = $recurso_idioma->getArrayMultimedia();
            $html .= '<div class="row">';
            foreach ($a_multimedia as $multimedia){
                $html .= VistaMultimedia::muestraMultimedia($multimedia);
            }
            $html .= '</div>';
        $html .= "</div>";
        $html .= "</div>";
        return $html;
    }
}
