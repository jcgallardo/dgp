<?php
    class VistaLogin{
        static function printLogin($mensaje){
            $html = "<div class='cont_login container'>";
            $html .= self::printTitle();
            $html .= self::printFormLogin($mensaje);
            $html .= "</div>";
            
            return $html;
        }
        static function printTitle(){
            $html = "<div class='cont_title'>";
                $html .= "<img src='https://granadaempresas.es/files/bmncajagranada--tiene-nueva-imagen-de-marca-0.jpg' width='150' height='100' alt='logo caja granada' />";
                $html .= "<h1>Museo Caja Granada</h1>";
                $html .= "<h2>-Backend-</h2>";
            $html .= "</div>";
            
            return $html;
            
        }
        
        static function printFormLogin($mensaje=null){
            $html = "<div class='cont_form_login container'>";
                $html .= "<form method='post' action='index.php' role='form' class='form_login'>";
                    $html .= '<div class="form-group">';
                    $html .= "<label for='in_user'>Email</label>";
                    $html .= "<input id='in_user' class='form-control text-login' type='text' name='email' placeholder='su-email@email.com' >";
                    $html .= '</div>';
                    $html .= '<div class="form-group">';
                    $html .= "<label for='in_pass'>Contraseña</label>";
                    $html .= "<input id='in_pass' class='form-control text-login' type='password' name='pass' placeholder='Escriba aquí su contraseña...' >";
                    $html .= '</div>';
                    if ($mensaje != null){
                        $html .= "<p class='error_message'>".$mensaje."</p>";
                    }
                    $html .= "<button class='btn btn-default' type='submit' >Iniciar sesión</button>";
                $html .= "</form>";
            $html .= "</div>";
            return $html;
        }
    }
?>