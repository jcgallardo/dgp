<?php
    class VistaMultimedias{
        
        
        static function formularioFiltroMultimedias($idiomas,$tipos){
            $html = "<div class='cont-form-multimedias'>";
                $html .= "<form class='form-filtro-multimedias' role='form' method='get' action='index.php'>";
                    $html .= "<p>-Filtra los medios por los siguientes campos</p>";
                    $html .= '<input type="hidden" name="section" value="medios" />';
                    $html .= "<div class='form-group'>";
                        $html .= '<label for="i_tipo_multimedia">Tipo de multimedia</label>';
                        $html .= '<select class="form-control" name="tipo" id="i_tipo_multimedia">';
                        $html .= '<option value="">todos los tipos</option>';
                        foreach ($tipos as $i => $value){
                            $html .= '<option value="'.$i.'">'.$value.'</option>';
                        }
                        $html .= '</select>';
                    $html .=  '</div>';
                    $html .= "<div class='form-group'>";
                        $html .= '<label for="i_idioma_multimedia">Idioma de multimedia</label>';
                        $html .= '<select class="form-control" name="idioma" id="i_idioma_multimedia">';
                        $html .= '<option value="">todos los idiomas</option>';
                        foreach ($idiomas as $i => $value){
                            $html .= '<option value="'.$i.'">'.$value.'</option>';
                        }
                        $html .= '</select>';
                    $html .=  '</div>';
                    $html .= '<button type="submit" class="btn btn-success">Filtrar medios</button>'; 
                $html .= "</form>";
            $html .= "</div>";
            
            echo $html;
        }

    public static function muestraMultimediasTabla($medios) {
        $html = "<div class='tabla-medios table-responsive'>";
            $html .= "<p class='info-busqueda-medios'>Se han encontrado ".count($medios)." resultados...</p>";
            $html .= "<table class='table table-hover'>";
                $html .= "<thead class='thead-default'>";
                    $html .= "<tr>";
                        $html .= "<th class='col-sm-1'>ID</th>";
                        $html .= "<th class='col-sm-1'>ID_IDIOMA</th>";
                        $html .= "<th class='col-sm-1'>ID_RECURSO</th>";                       
                        $html .= "<th class='col-sm-3'>CONTENIDO</th>";
                        $html .= "<th class='col-sm-1'>TIPO</th>";
                        $html .= "<th class='col-sm-3'>DESCRIPCION</th>";
                        $html .= "<th class='col-sm-1'></th>";
                        $html .= "<th class='col-sm-1'></th>";
                    $html .= "</tr>";
                $html .= "</thead>";
                $html .= "<tbody>";
                $html .= VistaMultimedias::muestraNuevoMultimedia();
                foreach ($medios as $medio) {
                    $html .= VistaMultimedia::muestraMultimediaFila($medio);
                }
                $html .= "</tbody>";
            $html .= "</table>";
            $html .= "</div>";
            echo $html;
    }

    public static function muestraNuevoMultimedia() {
        $html = "<tr>";
        //$html .= "<form action='index.php' method='get' role='form'>";
        $html .= '<input type="hidden" name="section" value="medios" />';
        $html .= '<td><input type="text" class="form-control" placeholder="automatico" disabled /></td>';
        $html .= '<td><input type="text" class="form-control" placeholder="Idioma"></td>';
        $html .= '<td><input type="text" class="form-control" placeholder="Recurso"></td>';
        $html .= '<td><input type="text" class="form-control" placeholder="Contenido"></td>';
        $html .= '<td><input type="text" class="form-control" placeholder="URL"></td>';
        $html .= '<td><input type="text" class="form-control" placeholder="Descripcion"></td>';
        $html .= "<td><button type='submit' name='accion' value='new' class='btn btn-primary'>+</button></td>";
        //$html .= "</form>";
        $html .= "</tr>";
        return $html;
    }

}

    
