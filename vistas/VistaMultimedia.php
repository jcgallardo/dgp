<?php
    class VistaMultimedia{
        static function muestraMultimedia($multimedia){
            $html = '<div class="col-sm-4 medio">';
            $html .= '<div class="botones_multimedia">';
            $html .= "<form role='form' action='index.php' method='get'>";
                //$html .= '<button class="btn btn-warning btn-editar-sala" name="editar_qr" type="submit" onclick="");">Editar</button>';
                $html .= '<button class="btn btn-danger" name="editar_qr" type="submit">Desvincular</button>';
                $html .= "<input type='hidden' name='id_medio' value='".$multimedia->getId()."'>";
                $html .= "<input type='hidden' name='id_sala' value='".$_SESSION['id_sala']."'>";
                $html .= "<input type='hidden' name='section' value='eliminar_medio'>";
            $html .= "</form>";
            $html .= '</div>';
            if ($multimedia->getId_tipo() == 1){
                $html .= '<p>'.$multimedia->getContenido().'</p>';
            }
            elseif ($multimedia->getId_tipo() == 2) {
                $html .= '<p>Audio asociado: </p>';
                $html .= '<audio controls>
                            <source src="'.$multimedia->getContenido().'" type="audio/mpeg">
                          Tu navegador no soporta la etiqueta audio
                        </audio>';
            }elseif ($multimedia->getId_tipo() == 3){
                $html .= '<video height="170" controls>
                            <source src="'.$multimedia->getContenido().'" type="video/mp4">
                          Your browser does not support the video tag.
                          </video>';
            }
            $html .= '</div>';
            return $html;
        }

    public static function muestraMultimediaFila($medio) {
        $html = "<tr>";
            $html .= "<form action='index.php' method='get' role='form'>";
            $html .= '<input type="hidden" name="section" value="medios" />';
            $html .= '<input type="hidden" name="id" value="'.$medio->getId().'" />';
            $html .= "<td><input type='text' class='form-control' value='".$medio->getId()."' disabled /></td>";
            $html .= "<td><input type='text' class='form-control' value='".$medio->getId_idioma()."' disabled></td>";
            $html .= "<td><input type='text' class='form-control' name='id_recurso' value='".$medio->getId_recurso()."' ></td>";           
            $html .= "<td><input type='text' class='form-control' name='contenido' value='".$medio->getContenido()."' ></td>";
            $html .= "<td><input type='text' class='form-control' name='tipo' value='".$medio->getId_tipo()."' disabled></td>";
            $html .= "<td><input type='text' class='form-control' name='descripcion' value='".$medio->getDescripcion()."'></td>";
            $html .= "<td><button type='submit' name='accion' value='update' class='btn btn-warning'>Modificar</button></td>";
            $html .= "<td><button type='submit' name='accion' value='delete' class='btn btn-danger'>Eliminar</button></td>";
            $html .= "</form>";
        $html .= "</tr>";
        
        return $html;
    }

    public static function mensajeExito($mensaje) {
        $html = "<div class='msg-accion alert alert-success'>";
        $html .= "<strong>¡Éxito!</strong> ".$mensaje;
        $html .= "</div>";
        echo $html;
    }

    public static function mensajeError($mensaje) {
        $html = "<div class='msg-accion alert alert-danger'>";
        $html .= "<strong>¡Error! </strong> ".$mensaje;
        $html .= "</div>";
        echo $html;
    }

}
?>

