<?php
    class VistaRecurso{
        static function muestraRecurso($recurso){        
             $html = "<div class='qr'>";
                $html .= "<div class='cont_buttons'>";
                $html .= "</div>";
                $html .= "<h3>(Id. ".$recurso->getId().") ". $recurso->getInformacion(). "</h3>";
                $html .= "<div class='row'>";
                    $html .= "<div class='im_qr col-md-2'><img alt='imagen de qr' src='".$recurso->getFigura()."' width='140' height='120' />";
                    $html .= "<form role='form' action='index.php' method='get'>";
                        $html .= "<button class='btn btn-warning modificar-code-qr' name='modificar' type='button' data-toggle='modal' data-target='#emergente_modificar'>Modificar código</button>";
                        $html .= "<div id='emergente_modificar' class='modal fade' role='dialog'>";
                        $html .= "<div class='modal-dialog'>";
                        $html .= "<div class='modal-content'>";
                        $html .= "<div class='modal-header'>";
                        $html .= "<button type='button' class='close' data-dismiss='modal'>&times;</button>";
                        $html .= "<h4 class='modal-tittle'> Modificar codigo QR</h4>";
                        $html .="</div>";
                        $html .= "<div class='modal-body'>";
                        $html .= "<div class='form-group'>";
                        $html .= "<label for='rec_qr'>Imagen QR</label>";
                        $html .= "<input type='file' name='im_qr' id='rec_qr'>";   
                        $html .= "</div>";
                        $html .= "<input type='hidden' id='h_id_recurso' name='id_recurso' value='".$recurso->getId()."' />";
                        $html .= "<input type='hidden' id='h_id_sala' name='section' value='modificar_codigo' />";
                        $html .= "<button class='btn btn-success' type='button' id='b_modificar_codigo'>Modificar codigo</button>";
                        $html .= "</div>";
                        $html .="</div>";
                        $html .="</div>";
                        $html .="</div>";
                        $html .= "<button class='btn btn-danger eliminar_qr' name='boton' value='eliminar' type='submit'>Eliminar recurso</button>";
                        $html .= "<input type='hidden' name='id_recurso' value='".$recurso->getId()."'>";
                        $html .= "<input type='hidden' name='section' value='eliminar_recurso'>";
                        $html .= "<input type='hidden' name='id_sala' value='".$recurso->getId_sala()."'>";
                        $html .= "</form>";
                    $html .= "</div>";
                    $html .= "<div class='medios_qr col-md-10'>";
                    $recursos_idioma = $recurso->getArrayRecursosIdioma();
                    foreach ($recursos_idioma as $recurso_idioma){
                        $html .= VistaRecursoIdioma::printRecursoIdioma($recurso_idioma);
                    }
                    $html .= "</div>";
                $html .= "</div>";
            $html .= "</div>";
            
            return $html;
        }

    public static function muestraRecursoFila($recurso) {
        $html = "<tr>";
            $html .= "<form action='index.php' method='get' role='form'>";
            $html .= '<input type="hidden" name="section" value="recursos" />';
            $html .= '<input type="hidden" name="id" value="'.$recurso->getId().'" />';
            $html .= "<td><input type='text' class='form-control' value='".$recurso->getId()."' disabled /></td>";
            $html .= "<td><input type='text' class='form-control' value='".$recurso->getIdTipo()."' disabled></td>";
            $html .= "<td><input type='text' class='form-control' name='id_sala' value='".$recurso->getId_sala()."' ></td>";           
            $html .= "<td><input type='text' class='form-control' name='info' value='".$recurso->getInformacion()."' ></td>";
            $html .= "<td><input type='text' class='form-control' name='figura' value='".$recurso->getFigura()."'></td>";
            $html .= "<td><button type='submit' name='accion' value='update' class='btn btn-warning'>Modificar</button></td>";
            $html .= "<td><button type='submit' name='accion' value='delete' class='btn btn-danger'>Eliminar</button></td>";
            $html .= "</form>";
        $html .= "</tr>";
        
        return $html;
    }

    public static function mensajeExito($mensaje) {
        $html = "<div class='msg-accion alert alert-success'>";
        $html .= "<strong>¡Éxito!</strong> ".$mensaje;
        $html .= "</div>";
        echo $html;
    }
    public static function mensajeError($mensaje) {
        $html = "<div class='msg-accion alert alert-danger'>";
        $html .= "<strong>¡Error! </strong> ".$mensaje;
        $html .= "</div>";
        echo $html;
    }

    public static function muestraRecursoNuevo($tipos) {
        $html = "<tr>";
        $html .= "<form action='index.php' method='get' role='form'>";
        $html .= '<input type="hidden" name="section" value="recursos" />';
        $html .= "<td><input type='text' class='form-control' placeholder='automatico' disabled /></td>";
        $html .= '<td><select class="form-control" name="tipo" id="i_tipo_recurso">';
        foreach ($tipos as $i => $value){
            $html .= '<option value="'.$i.'">'.$value.'</option>';
        }
        $html .= '</select></td>';
        $html .= "<td><input type='text' class='form-control' name='id_sala' placeholder='Id de sala' ></td>";           
        $html .= "<td><input type='text' class='form-control' name='info' placeholder='Aquí la información sobre el recurso' ></td>";
        $html .= "<td><input type='text' class='form-control' name='figura' placeholder='URL de la figura'></td>";
        $html .= "<td><button type='submit' name='accion' value='new' class='btn btn-primary'>+</button></td>";
        $html .= "</form>";
        $html .= "</tr>";
        return $html;
    }

}
?>
