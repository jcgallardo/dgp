<?php
    class VistaRecursos{
        static function muestraRecursos($recursos){
            echo "<div class='contenedor_qrs'>";
                foreach ($recursos as $recurso) {
                    echo VistaRecurso::muestraRecurso($recurso);
                }
            echo "</div>";
        }
        static function muestraRecursosTabla($recursos,$tipos){
            $html = "<div class='tabla-recursos table-responsive'>";
            $html .= "<p class='info-busqueda-recurso'>Se han encontrado ".count($recursos)." resultados...</p>";
            $html .= "<table class='table table-hover'>";
                $html .= "<thead class='thead-default'>";
                    $html .= "<tr>";
                        $html .= "<th class='col-sm-1'>ID</th>";
                        $html .= "<th class='col-sm-2'>TIPO</th>";
                        $html .= "<th class='col-sm-1'>ID SALA</th>";                       
                        $html .= "<th class='col-sm-3'>INFO</th>";
                        $html .= "<th class='col-sm-3'>IMAGEN QR</th>";
                        $html .= "<th class='col-sm-1'></th>";
                        $html .= "<th class='col-sm-1'></th>";
                    $html .= "</tr>";
                $html .= "</thead>";
                $html .= "<tbody>";
                $html .= VistaRecurso::muestraRecursoNuevo($tipos);
                foreach ($recursos as $recurso) {
                    $html .= VistaRecurso::muestraRecursoFila($recurso);
                }
                $html .= "</tbody>";
            $html .= "</table>";
            $html .= "</div>";
            echo $html;
        }
        static function formularioFiltroRecursos($array_tipos){
            $html = "<div class='cont-form-recursos'>";
                $html .= "<form class='form-filtro-recursos' role='form' method='get' action='index.php'>";
                    $html .= "<p>-Filtra los recursos por los siguientes campos</p>";
                    $html .= '<input type="hidden" name="section" value="recursos" />';
                    $html .= "<div class='form-group'>";
                        $html .= '<label for="i_id_sala">ID Sala</label>';
                        $html .= '<input name="id_sala" type="text" class="form-control" id="i_id_sala" placeholder="Introduce la id de sala">';
                    $html .=  '</div>';
                    $html .= "<div class='form-group'>";
                        $html .= '<label for="i_tipo_recurso">Tipo de recurso</label>';
                        $html .= '<select class="form-control" name="tipo" id="i_tipo_recurso">';
                        $html .= '<option value="">todos los tipos</option>';
                        foreach ($array_tipos as $i => $value){
                            $html .= '<option value="'.$i.'">'.$value.'</option>';
                        }
                        $html .= '</select>';
                    $html .=  '</div>';
                    $html .= '<button type="submit" class="btn btn-success">Filtrar recursos</button>'; 
                $html .= "</form>";
            $html .= "</div>";
            
            echo $html;
        }
    }

    
