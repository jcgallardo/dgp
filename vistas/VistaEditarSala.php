<?php
//include "VistaSala.php";


class VistaEditarSala {


    static function muestraSala($sala) {
        $html = "<div class='contenedor_salas'>";
        $html .= "<div class='editar_sala'>";
                $html .= "<div class='imagen_sala' style='background-image:url(".$sala->getImagen().");'></div>";
                $html .= "<h1>". $sala->getNombre(). " : " .$sala->getDescripcion() ."</h1>";
                $html .= "<button class='btn btn-success boton_nuevo_recurso' type='button' class='nuevo_recurso' data-toggle='modal' data-target='#emergente'>Añadir Recurso</button>";
                $html .= "<div id='emergente' class='modal fade' role='dialog'>";
                $html .= "<div class='modal-dialog'>";
                $html .= "<div class='modal-content'>";
                $html .= "<div class='modal-header'>";
                $html .= "<button type='button' class='close' data-dismiss='modal'>&times;</button>";
                $html .= "<h4 class='modal-tittle'> Crea un nuevo recurso</h4>";
                $html .="</div>";
                $html .= "<div class='modal-body'>";
                $html .= "<form role='form' action='index.php' method='post' >";
                $html .= "<div class='form-group'>";
                $html .= "<label for='tit_recurso'>Titulo recurso</label>";
                $html .= "<input name='tit_recurso' type='text' class='form-control' id='tit_recurso' placeholder='Titulo del recurso'>";
                $html .= "</div>";
                $html .= "<div class='form-group'>";
                $html .= "<label for='rec_qr'>Imagen QR</label>";
                $html .= "<input type='file' name='im_qr' id='rec_qr'>";   
                $html .= "</div>";
                $html .= "<input type='hidden' id='h_id_sala' name='id_sala' value='".$sala->getId()."' />";
                $html .= "<input type='hidden' id='h_id_sala' name='section' value='nuevo_recurso' />";
                $html .= "<button class='btn btn-success' type='submit' id='b_crear_recurso'>Crear recurso</button>";
                $html .= "</form>";
                $html .= "</div>";
                $html .="</div>";
                $html .="</div>";
                $html .="</div>";
                $html .= "<div class='cont_sala'>";
                
                $recursos = $sala->getArrayRecursos();
                foreach ($recursos as $recurso){
                    $html .= VistaRecurso::muestraRecurso($recurso);
                }
                $html .= "</div>";
            $html .= "</div>";
            $html .= "</div>";
            echo $html;
    }


}
?>