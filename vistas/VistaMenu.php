<?php
    class VistaMenu{
        static function printMenu($seccion){
            $html = '<nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Desplegar navegación</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>';
            
            $html .= '<div class="div_menu collapse navbar-collapse navbar-ex1-collapse"><ul class="ul_menu nav navbar-nav">';
            if ($seccion == "salas" || $seccion == "editarSala")
                $html .= "<li class='active'><a href='?section=salas'>Salas</a></li>";
            else
                $html .= "<li><a href='?section=salas'>Salas</a></li>";
            if ($seccion == "recursos")
                $html .= "<li class='active'><a href='?section=recursos'>Recursos</a></li>";
            else
                $html .= "<li><a href='?section=recursos'>Recursos</a></li>";
            if ($seccion == "medios")
                $html .= "<li class='active'><a href='?section=medios'>Medios</a></li>";
            else
                $html .= "<li><a href='?section=medios'>Medios</a></li>";
            
            $html .= '<li><a href="controlador/close_session.php" >Salir</a></li>';
            
            $html .= '</ul></div></nav>';
            
            return $html;
        }
    }
?>