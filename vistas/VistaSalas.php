<?php
    include "VistaSala.php";
    
    class VistaSalas{
        static function muestraSalas($salas){
            echo "<div class='contenedor_salas'>";
                echo VistaSala::formAniadirSala();
                foreach ($salas as $sala) {
                    echo VistaSala::muestraSala($sala);
                }
            echo "</div>";
            echo "<script>";
            echo "function redirigir_sala(id){";
            echo "    window.location = 'index.php?section=editarSala&id_sala='+id;";
            echo "}";
            echo "</script>";
        }

    public static function mensajeExito($mensaje) {
        $html = "<div class='msg-accion alert alert-success'>";
        $html .= "<strong>¡Éxito!</strong> ".$mensaje;
        $html .= "</div>";
        echo $html;
    }
    public static function mensajeError($mensaje) {
        $html = "<div class='msg-accion alert alert-danger'>";
        $html .= "<strong>¡Error! </strong> ".$mensaje;
        $html .= "</div>";
        echo $html;
    }

}