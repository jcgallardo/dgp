<?php
    
    class Connection{
        private $_connection;
        static $_dbinstance;
        const DB_SERVER="eu-cdbr-azure-west-a.cloudapp.net";
        const DB_NAME="museo_db";
        const DB_USER="b04083b6553b54";
        const DB_PASS="e9a94773";
        
        private function __construct(){
            mysqli_report(MYSQLI_REPORT_STRICT);

            try{
                $this->_connection = new mysqli(self::DB_SERVER, self::DB_USER, self::DB_PASS, self::DB_NAME);

                //para solucionar codificaciones
                $this->_connection->query("SET NAMES 'utf8' ");

            }catch (Exception $e ) {
                $this->connection = null;
                echo $e->getMessage();
           }
        }
        
        public static function cerrarConexion(){
            if (!is_null(self::$_dbinstance)) {
                self::$_dbinstance->_connection->close();
            }
        }
        
        public static function getConnection(){
            if (is_null(self::$_dbinstance)) {
                self::$_dbinstance = new Connection();
            }
            return self::$_dbinstance->_connection;
        }
        
    }