<!DOCTYPE html>
<html lang="es-ES">
<?php
    session_start();
    include "./conexionDB/connection.php";
    include "./head.php";
    echo "<body>";

    if (isset($_SESSION['logged']) && $_SESSION['logged'] == true){
        $seccion = "salas";
        
        if (isset($_GET['section']))
            $seccion = $_GET['section'];
        if (isset($_POST['section']))
            $seccion = $_POST['section'];
        
        include "./controlador/header.php";
        include "./controlador/contenido.php";
    }else{
        include "./controlador/login.php";
        // hola
    }
?>
</body>
</html>